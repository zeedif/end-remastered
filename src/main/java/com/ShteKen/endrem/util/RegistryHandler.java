package com.ShteKen.endrem.util;

import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.blocks.*;
import com.ShteKen.endrem.items.*;
import net.minecraft.block.*;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class RegistryHandler {

    //where every blocks and items are registered and might register the biomes here later

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, EndRemastered.MOD_ID);
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, EndRemastered.MOD_ID);
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, EndRemastered.MOD_ID);

    public static Item[] PEARL_ARRAY = new Item[11];
    public static Block[] FRAME_ARRAY = new Block[11];

    public static void init() {
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        SOUNDS.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static void registerArrays(){

        PEARL_ARRAY[0] = OLD_PEARL.get();
        PEARL_ARRAY[1] = ROGUE_PEARL.get();
        PEARL_ARRAY[2] = NETHER_PEARL.get();
        PEARL_ARRAY[3] = COLD_PEARL.get();
        PEARL_ARRAY[4] = CORRUPTED_PEARL.get();
        PEARL_ARRAY[5] = MAGICAL_PEARL.get();
        PEARL_ARRAY[6] = BLACK_PEARL.get();
        PEARL_ARRAY[7] = LOST_PEARL.get();
        PEARL_ARRAY[8] = WITHER_PEARL.get();
        PEARL_ARRAY[9] = END_CRYSTAL_PEARL.get();
        PEARL_ARRAY[10] = GUARDIAN_PEARL.get();

        FRAME_ARRAY[0] = OLD_FRAME.get();
        FRAME_ARRAY[1] = ROGUE_FRAME.get();
        FRAME_ARRAY[2] = NETHER_FRAME.get();
        FRAME_ARRAY[3] = COLD_FRAME.get();
        FRAME_ARRAY[4] = CORRUPTED_FRAME.get();
        FRAME_ARRAY[5] = MAGICAL_FRAME.get();
        FRAME_ARRAY[6] = BLACK_FRAME.get();
        FRAME_ARRAY[7] = LOST_FRAME.get();
        FRAME_ARRAY[8] = WITHER_FRAME.get();
        FRAME_ARRAY[9] = END_CRYSTAL_FRAME.get();
        FRAME_ARRAY[10] = GUARDIAN_FRAME.get();
    }

    //Item
    public static final RegistryObject<Item> OLD_PEARL = ITEMS.register("old_pearl", CustomPearl::new);
    public static final RegistryObject<Item> ROGUE_PEARL = ITEMS.register("rogue_pearl", CustomPearl::new);
    public static final RegistryObject<Item> NETHER_PEARL = ITEMS.register("nether_pearl", CustomPearl::new);
    public static final RegistryObject<Item> COLD_PEARL = ITEMS.register("cold_pearl", CustomPearl::new);
    public static final RegistryObject<Item> CORRUPTED_PEARL = ITEMS.register("corrupted_pearl", CustomPearl::new);
    public static final RegistryObject<Item> MAGICAL_PEARL = ITEMS.register("magical_pearl", CustomPearl::new);
    public static final RegistryObject<Item> BLACK_PEARL = ITEMS.register("black_pearl", CustomPearl::new);
    public static final RegistryObject<Item> LOST_PEARL = ITEMS.register("lost_pearl", CustomPearl::new);
    public static final RegistryObject<Item> WITHER_PEARL = ITEMS.register("wither_pearl", CustomPearl::new);
    public static final RegistryObject<Item> END_CRYSTAL_PEARL = ITEMS.register("end_crystal_pearl", CustomPearl::new);
    public static final RegistryObject<Item> GUARDIAN_PEARL = ITEMS.register("guardian_pearl", CustomPearl::new);
    public static final RegistryObject<Item> END_CRYSTAL_FRAGMENT = ITEMS.register("end_crystal_fragment", ItemBase::new);

    public static final RegistryObject<Item> CORE = ITEMS.register("core", ItemBase::new);
    public static final RegistryObject<Item> POWERED_CORE = ITEMS.register("powered_core", ItemBase::new);
    public static final RegistryObject<Item> END_CRYSTAL_HORSE_ARMOR = ITEMS.register("end_crystal_horse_armor", EndCrystalHorseArmor::new);

    public static final RegistryObject<Item> END_CASTLE_DISC_ITEM = ITEMS.register("end_castle_disc", () -> new EndMusicDisc(1, RegistryHandler.END_CASTLE_MUSIC.get(), new Item.Properties().maxStackSize(1).group(EndRemastered.TAB)));

    //Block
    public static final RegistryObject<Block> END_CREATOR = BLOCKS.register("end_creator", EndCreator::new);
    public static final RegistryObject<Block> END_CREATOR_ACTIVATED = BLOCKS.register("end_creator_activated", EndCreatorActivated::new);

    public static final RegistryObject<Block> OLD_FRAME = BLOCKS.register("old_frame", FrameClass::new);
    public static final RegistryObject<Block> ROGUE_FRAME = BLOCKS.register("rogue_frame", FrameClass::new);
    public static final RegistryObject<Block> NETHER_FRAME = BLOCKS.register("nether_frame", FrameClass::new);
    public static final RegistryObject<Block> COLD_FRAME = BLOCKS.register("cold_frame", FrameClass::new);
    public static final RegistryObject<Block> CORRUPTED_FRAME = BLOCKS.register("corrupted_frame", FrameClass::new);
    public static final RegistryObject<Block> MAGICAL_FRAME = BLOCKS.register("magical_frame", FrameClass::new);
    public static final RegistryObject<Block> BLACK_FRAME = BLOCKS.register("black_frame", FrameClass::new);
    public static final RegistryObject<Block> LOST_FRAME = BLOCKS.register("lost_frame", FrameClass::new);
    public static final RegistryObject<Block> WITHER_FRAME = BLOCKS.register("wither_frame", FrameClass::new);
    public static final RegistryObject<Block> END_CRYSTAL_FRAME = BLOCKS.register("end_crystal_frame", FrameClass::new);
    public static final RegistryObject<Block> GUARDIAN_FRAME = BLOCKS.register("guardian_frame", FrameClass::new);
    public static final RegistryObject<Block> EMPTY_FRAME = BLOCKS.register("empty_frame", EmptyFrame::new);

    public static final RegistryObject<Block> FAKE_STONE = BLOCKS.register("fake_stone", () -> new FakeStone(Block.Properties.from(Blocks.STONE)));
    public static final RegistryObject<Block> END_CRYSTAL_BLOCK = BLOCKS.register("end_crystal_block", EndCrystalBlock::new);
    public static final RegistryObject<Block> END_CRYSTAL_ORE = BLOCKS.register("end_crystal_ore", () -> new OreBlockBase(Block.Properties.from(Blocks.ANCIENT_DEBRIS)));



    //Block Item

    public static final RegistryObject<Item> END_CREATOR_ITEM = ITEMS.register("end_creator", () -> new BlockItem(END_CREATOR.get(), new Item.Properties().group(EndRemastered.TAB)));
    public static final RegistryObject<Item> EMPTY_FRAME_ITEM = ITEMS.register("empty_frame", () -> new BlockItem(EMPTY_FRAME.get(), new Item.Properties().group(EndRemastered.TAB)));

    public static final RegistryObject<Item> END_CREATOR_ACTIVATED_ITEM = ITEMS.register("end_creator_activated", () -> new BlockItem(END_CREATOR_ACTIVATED.get(), new Item.Properties()));
    public static final RegistryObject<Item> OLD_FRAME_ITEM = ITEMS.register("old_frame", () -> new BlockItem(OLD_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> ROGUE_FRAME_ITEM = ITEMS.register("rogue_frame", () -> new BlockItem(ROGUE_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> NETHER_FRAME_ITEM = ITEMS.register("nether_frame", () -> new BlockItem(NETHER_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> COLD_FRAME_ITEM = ITEMS.register("cold_frame", () -> new BlockItem(COLD_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> CORRUPTED_FRAME_ITEM = ITEMS.register("corrupted_frame", () -> new BlockItem(CORRUPTED_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> MAGICAL_FRAME_ITEM = ITEMS.register("magical_frame", () -> new BlockItem(MAGICAL_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> BLACK_FRAME_ITEM = ITEMS.register("black_frame", () -> new BlockItem(BLACK_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> LOST_FRAME_ITEM = ITEMS.register("lost_frame", () -> new BlockItem(LOST_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> WITHER_FRAME_ITEM = ITEMS.register("wither_frame", () -> new BlockItem(WITHER_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> END_CRYSTAL_FRAME_ITEM = ITEMS.register("end_crystal_frame", () -> new BlockItem(END_CRYSTAL_FRAME.get(), new Item.Properties()));
    public static final RegistryObject<Item> GUARDIAN_FRAME_ITEM = ITEMS.register("guardian_frame", () -> new BlockItem(GUARDIAN_FRAME.get(), new Item.Properties()));

    public static final RegistryObject<Item> FAKE_STONE_ITEM = ITEMS.register("fake_stone", () -> new BlockItem(FAKE_STONE.get(), new Item.Properties()));
    public static final RegistryObject<Item> END_CRYSTAL_BLOCK_ITEM = ITEMS.register("end_crystal_block", () -> new BlockItem(END_CRYSTAL_BLOCK.get(), new Item.Properties().group(EndRemastered.TAB)));
    public static final RegistryObject<Item> END_CRYSTAL_ORE_ITEM = ITEMS.register("end_crystal_ore", () -> new BlockItem(END_CRYSTAL_ORE.get(), new Item.Properties().group(EndRemastered.TAB)));

    //Sounds

    public static final RegistryObject<SoundEvent> PORTAL_OPEN_SOUND = SOUNDS.register("music.portal_open_sound", () -> new SoundEvent(new ResourceLocation(EndRemastered.MOD_ID, "music.portal_open_sound")));
    public static final Lazy<SoundEvent> END_CASTLE_MUSIC = Lazy.of(() -> new SoundEvent(new ResourceLocation(EndRemastered.MOD_ID, "item.end_castle_disc")));
    public static final RegistryObject<SoundEvent> END_CASTLE_DISC_MUSIC = SOUNDS.register("item.end_castle_disc.disc", END_CASTLE_MUSIC);

    // Armor
    public static final RegistryObject<Item> END_CRYSTAL_HELMET = ITEMS.register("end_crystal_helmet", () -> new EndCrystalArmor(EquipmentSlotType.HEAD));
    public static final RegistryObject<Item> END_CRYSTAL_CHESTPLATE = ITEMS.register("end_crystal_chestplate", () -> new EndCrystalArmor(EquipmentSlotType.CHEST));
    public static final RegistryObject<Item> END_CRYSTAL_LEGGINGS = ITEMS.register("end_crystal_leggings", () -> new EndCrystalArmor(EquipmentSlotType.LEGS));
    public static final RegistryObject<Item> END_CRYSTAL_BOOTS = ITEMS.register("end_crystal_boots", () -> new EndCrystalArmor(EquipmentSlotType.FEET));

    // Tools
    public static final RegistryObject<Item> END_CRYSTAL_HOE = ITEMS.register("end_crystal_hoe", EndCrystalTool.EndCrystalHoe::new);
    public static final RegistryObject<Item> END_CRYSTAL_PICKAXE = ITEMS.register("end_crystal_pickaxe", EndCrystalTool.EndCrystalPickaxe::new);
    public static final RegistryObject<Item> END_CRYSTAL_AXE = ITEMS.register("end_crystal_axe", EndCrystalTool.EndCrystalAxe::new);
    public static final RegistryObject<Item> END_CRYSTAL_SWORD = ITEMS.register("end_crystal_sword", EndCrystalTool.EndCrystalSword::new);
    public static final RegistryObject<Item> END_CRYSTAL_SHOVEL = ITEMS.register("end_crystal_shovel", EndCrystalTool.EndCrystalShovel::new);
}
