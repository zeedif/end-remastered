package com.ShteKen.endrem.items;

import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.config.Config;
import com.ShteKen.endrem.world.STConfig.STJStructures;
import com.ShteKen.endrem.world.STConfig.STStructures;
import net.minecraft.entity.Entity;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.item.FilledMapItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.MapData;
import net.minecraft.world.storage.MapDecoration;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.event.village.WandererTradesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.loading.FMLPaths;

import javax.annotation.Nonnull;
import java.util.Random;

@Mod.EventBusSubscriber
public class CustomMap {
    private static final int minPrice = 10;
    private static final int maxPrice = 20;
    private static final int experienceGiven = 20;

    public static Structure<NoFeatureConfig> getStructureToLocate() {
        Config.setup(FMLPaths.CONFIGDIR.get().resolve(EndRemastered.MOD_ID + ".toml"));

        switch (Config.MAP_LOCATES_STRUCTURE.get()) {
            case 0:
                return null;
            case 1: default:
                return Config.END_CASTLE_ENABLED.get() ? STStructures.END_CASTLE.get() : null;
            case 2:
                return Config.END_GATE_ENABLED.get() ? STJStructures.END_GATE.get() : null;
        }
    }


    public static ItemStack createMap(World world, BlockPos position) {

        if(!(world instanceof ServerWorld))
            return ItemStack.EMPTY;

        ServerWorld serverWorld = (ServerWorld) world;

        BlockPos structurePos = serverWorld.getChunkProvider().getChunkGenerator().func_235956_a_(serverWorld, getStructureToLocate(), position, 100, false);


        if(structurePos == null)
            return ItemStack.EMPTY;

        ItemStack stack = FilledMapItem.setupNewMap(world, structurePos.getX(), structurePos.getZ(), (byte) 2, true, true);
        // fillExplorationMap
        FilledMapItem.func_226642_a_((ServerWorld) world, stack);
        MapData.addTargetDecoration(stack, structurePos, "+", MapDecoration.Type.TARGET_X);
        if (getStructureToLocate() == STJStructures.END_GATE.get()){
            stack.setDisplayName(new TranslationTextComponent("item.endrem.end_gate_map"));
        } else if (getStructureToLocate() == STStructures.END_CASTLE.get()) {
            stack.setDisplayName(new TranslationTextComponent("item.endrem.end_castle_map"));
        }
        return stack;
    }

    @SubscribeEvent
    public static void villagerTrade(VillagerTradesEvent event) {
        if (event.getType() == VillagerProfession.CARTOGRAPHER) {
            if (getStructureToLocate() != null) {
                event.getTrades().get(3).add(new CustomMapTrade());
            }
        }
    }

    @SubscribeEvent
    public static void wandererTrades(WandererTradesEvent event) {
        if (getStructureToLocate() != null) {
            event.getGenericTrades().add(new CustomMapTrade());
        }
    }

    private static class CustomMapTrade implements VillagerTrades.ITrade {

        @Override
        public MerchantOffer getOffer(@Nonnull Entity entity, Random random){
            int priceEmeralds = random.nextInt(maxPrice - minPrice + 1) + minPrice;
            ItemStack map = createMap(entity.world, entity.getPosition());

            if (map != ItemStack.EMPTY) {
                return new MerchantOffer(new ItemStack(Items.EMERALD, priceEmeralds), new ItemStack(Items.COMPASS), map, 12, experienceGiven, 0.2F);
            }
            return null;
        }
    }
}
