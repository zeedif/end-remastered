package com.ShteKen.endrem.world.structures;


import com.ShteKen.endrem.config.Config;
import com.mojang.serialization.Codec;
import net.minecraft.util.Rotation;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.registry.DynamicRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureStart;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraftforge.event.world.BiomeLoadingEvent;

public class EndCastle extends Structure<NoFeatureConfig> {
    private final int SPAWN_DISTANCE;

    public static Biome.Category[] VALID_BIOMES = new Biome.Category[]{
            Biome.Category.PLAINS,
            Biome.Category.JUNGLE,
            Biome.Category.TAIGA,
            Biome.Category.FOREST
    };

    public EndCastle(Codec<NoFeatureConfig> codec) {
        super(codec);
        this.SPAWN_DISTANCE = (int) (0.6 * Config.END_CASTLE_DISTANCE.get());
    }

    public static Boolean isValidSpawn(BiomeLoadingEvent event){
        for (Biome.Category biomeCategory : VALID_BIOMES){
            if (biomeCategory == event.getCategory()){
                return event.getDepth() < 0.3F;
            }
        }
        return false;
    }

    @Override
    public  IStartFactory<NoFeatureConfig> getStartFactory() {
        return EndCastle.Start::new;
    }

    @Override
    public GenerationStage.Decoration getDecorationStage()
    {
        return GenerationStage.Decoration.SURFACE_STRUCTURES;
    }

    @Override
    protected boolean func_230363_a_(ChunkGenerator chunkGenerator, BiomeProvider biomeSource, long seed, SharedSeedRandom chunkRandom, int chunkX, int chunkZ, Biome biome, ChunkPos chunkPos, NoFeatureConfig noFeatureConfig) {
        return getDistanceFromSpawn(chunkX, chunkZ) > this.SPAWN_DISTANCE;
    }

    protected int getDistanceFromSpawn(int chunkX, int chunkZ) {
        return (int) Math.sqrt(Math.pow(chunkX, 2) + Math.pow(chunkZ, 2)) << 4;
    }

    public static class Start extends StructureStart<NoFeatureConfig> {
        public Start(Structure<NoFeatureConfig> structureIn, int chunkX, int chunkZ, MutableBoundingBox mutableBoundingBox, int referenceIn, long seedIn) {
            super(structureIn, chunkX, chunkZ, mutableBoundingBox, referenceIn, seedIn);
        }

        @Override
        public void func_230364_a_(DynamicRegistries dynamicRegistryManager, ChunkGenerator generator, TemplateManager templateManagerIn, int chunkX, int chunkZ, Biome biomeIn, NoFeatureConfig config) {

            Rotation rotation = Rotation.values()[this.rand.nextInt(Rotation.values().length)];

            // Turns the chunk coordinates into actual coordinates we can use. (Gets center of that chunk)
            int x = (chunkX << 4) + 7;
            int z = (chunkZ << 4) + 7;

            // Finds the y value of the terrain at location.
            int surfaceY = generator.getHeight(x, z, Heightmap.Type.WORLD_SURFACE_WG);
            BlockPos genPosition = new BlockPos(x, surfaceY, z);

            EndCastlePieces.start(templateManagerIn, genPosition, rotation, this.components, this.rand);

            this.recalculateStructureSize();

//            EndRemastered.LOGGER.log(Level.DEBUG, String.format("End Castle at %s", genPosition.getCoordinatesAsString()));
        }
    }
}
