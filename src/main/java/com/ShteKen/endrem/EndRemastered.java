package com.ShteKen.endrem;

import com.ShteKen.endrem.config.Config;
import com.ShteKen.endrem.util.LootModifier;
import com.ShteKen.endrem.util.RegistryHandler;
import com.ShteKen.endrem.world.STConfig.STJConfiguredStructures;
import com.ShteKen.endrem.world.STConfig.STJStructures;
import com.ShteKen.endrem.world.gen.OreSpawnHandler;
import com.ShteKen.endrem.world.STConfig.STConfiguredStructures;
import com.ShteKen.endrem.world.STConfig.STStructures;
import com.google.common.collect.ImmutableMap;
import com.mojang.serialization.Codec;
import com.ShteKen.endrem.world.structures.EndCastle;
import com.ShteKen.endrem.world.structures.EndGate;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.World;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.FlatChunkGenerator;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


@Mod("endrem")
// The value here should match an entry in the META-INF/mods.toml file
public class EndRemastered {
    // Directly reference a log4j logger.
    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "endrem";
    public static final String NAME = "End Remastered";

    public EndRemastered() {
        // Load Config
        Config.setup(FMLPaths.CONFIGDIR.get().resolve(MOD_ID + ".toml"));

        RegistryHandler.init();
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);

        //////////////////////////////////////////////////////////////////////////////////////

        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        STJStructures.DEFERRED_REGISTRY_STRUCTURE.register(modEventBus);
        STStructures.DEFERRED_REGISTRY_STRUCTURE.register(modEventBus);
        modEventBus.addListener(this::setup);

        // For events that happen after initialization.
        IEventBus forgeBus = MinecraftForge.EVENT_BUS;
        forgeBus.addListener(EventPriority.NORMAL, this::addDimensionalSpacing);

        // The comments for BiomeLoadingEvent and StructureSpawnListGatherEvent says to do HIGH for additions.
        forgeBus.addListener(EventPriority.HIGH, this::biomeModification);

    }

    private void setup(final FMLCommonSetupEvent event) {
        event.enqueueWork(() -> {
            STStructures.setupStructures();
            STStructures.registerAllPieces();
            STConfiguredStructures.registerConfiguredStructures();

            STJStructures.setupStructures();
            STJConfiguredStructures.registerConfiguredStructures();

            WorldGenRegistries.NOISE_SETTINGS.getEntries().forEach(settings -> {
                Map<Structure<?>, StructureSeparationSettings> structureMap = settings.getValue().getStructures().func_236195_a_();

                if(structureMap instanceof ImmutableMap){
                    Map<Structure<?>, StructureSeparationSettings> tempMap = new HashMap<>(structureMap);
                    tempMap.put(STStructures.END_CASTLE.get(), DimensionStructuresSettings.field_236191_b_.get(STStructures.END_CASTLE.get()));
                    settings.getValue().getStructures().field_236193_d_ = tempMap;
                }
                else{
                    structureMap.put(STStructures.END_CASTLE.get(), DimensionStructuresSettings.field_236191_b_.get(STStructures.END_CASTLE.get()));
                }
            });

            WorldGenRegistries.NOISE_SETTINGS.getEntries().forEach(settings -> {
                Map<Structure<?>, StructureSeparationSettings> structureMap = settings.getValue().getStructures().func_236195_a_();

                if(structureMap instanceof ImmutableMap){
                    Map<Structure<?>, StructureSeparationSettings> tempMap = new HashMap<>(structureMap);
                    tempMap.put(STJStructures.END_GATE.get(), DimensionStructuresSettings.field_236191_b_.get(STJStructures.END_GATE.get()));
                    settings.getValue().getStructures().field_236193_d_ = tempMap;
                }
                else{
                    structureMap.put(STJStructures.END_GATE.get(), DimensionStructuresSettings.field_236191_b_.get(STJStructures.END_GATE.get()));
                }
            });
        });
        OreSpawnHandler.registerOres();
        RegistryHandler.registerArrays();
        MinecraftForge.EVENT_BUS.register(new LootModifier());
    }

    public void biomeModification(final BiomeLoadingEvent event) {
        // Add structure to all biomes
        BiomeGenerationSettingsBuilder generation = event.getGeneration();
        if (EndCastle.isValidSpawn(event) && Config.END_CASTLE_ENABLED.get()) {
            generation.getStructures().add(() -> (STConfiguredStructures.CONFIGURED_END_CASTLE));
        } else if (EndGate.isValidSpawn(event) && Config.END_GATE_ENABLED.get()) {
            generation.getStructures().add(() -> (STJConfiguredStructures.CONFIGURED_END_GATE));
        }
    }
    private static Method GETCODEC_METHOD;
    public void addDimensionalSpacing(final WorldEvent.Load event) {
        if (event.getWorld() instanceof ServerWorld) {
            ServerWorld serverWorld = (ServerWorld) event.getWorld();

            try {
                if(GETCODEC_METHOD == null) GETCODEC_METHOD = ObfuscationReflectionHelper.findMethod(ChunkGenerator.class, "func_230347_a_");
                ResourceLocation cgRL = Registry.CHUNK_GENERATOR_CODEC.getKey((Codec<? extends ChunkGenerator>) GETCODEC_METHOD.invoke(serverWorld.getChunkProvider().generator));
                if(cgRL != null && cgRL.getNamespace().equals("terraforged")) return;
            }
            catch(Exception e){
                EndRemastered.LOGGER.error("Was unable to check if " + serverWorld.getDimensionKey().getLocation() + " is using Terraforged's ChunkGenerator.");
            }
            // Prevent spawning our structure in Vanilla's superflat world

            if (serverWorld.getChunkProvider().getChunkGenerator() instanceof FlatChunkGenerator &&
                    serverWorld.getDimensionKey().equals(World.OVERWORLD)) {
                return;
            }

            Map<Structure<?>, StructureSeparationSettings> tempMap = new HashMap<>(serverWorld.getChunkProvider().generator.func_235957_b_().func_236195_a_());
            tempMap.putIfAbsent(STStructures.END_CASTLE.get(), DimensionStructuresSettings.field_236191_b_.get(STStructures.END_CASTLE.get()));
            serverWorld.getChunkProvider().generator.func_235957_b_().field_236193_d_ = tempMap;

            Map<Structure<?>, StructureSeparationSettings> tempMap2 = new HashMap<>(serverWorld.getChunkProvider().generator.func_235957_b_().func_236195_a_());
            tempMap2.putIfAbsent(STJStructures.END_GATE.get(), DimensionStructuresSettings.field_236191_b_.get(STJStructures.END_GATE.get()));
            serverWorld.getChunkProvider().generator.func_235957_b_().field_236193_d_ = tempMap2;

        }
    }

    public static <T extends IForgeRegistryEntry<T>> T register(IForgeRegistry<T> registry, T entry, String registryKey) {
        entry.setRegistryName(new ResourceLocation(EndRemastered.MOD_ID, registryKey));
        registry.register(entry);
        return entry;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    public static final ItemGroup TAB = new ItemGroup("endremTab") {

        @Override
        public ItemStack createIcon() {
            return new ItemStack(RegistryHandler.POWERED_CORE.get());

        }

    };
}
